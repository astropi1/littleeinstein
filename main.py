from pathlib import Path
from sense_hat import SenseHat
from logzero import logger, logfile
from time import sleep
from datetime import datetime, timedelta
from orbit import ISS
from gpiozero import CPUTemperature
import csv

#3 Media Scuola media Margherita Hack Castelnuovo (TE)
#Gruppo Little Einstein
#Tutor: Matteo Canzari - INAF - Osservatorio Astronomico d'Abruzzo

#function to create a cvs file
def create_csv_file(data_file):
    #create file using the data_file as filename
    with open(data_file, 'w') as f:
        writer = csv.writer(f)
        #Define the header
        header = ("Counter", "Date/time", "Latitude", "Longitude", "Temperature", "Humidity", "Pressure", "CPU Temperature")
        #write the header
        writer.writerow(header)

#function to write data in data_file
def add_csv_data(data_file, data):
    #open file
    with open(data_file, 'a') as f:
        #write data
        writer = csv.writer(f)
        writer.writerow(data)


#INITIALIZING VARIABLES

#counter to track the number of interation
counter1 = 0


#calculate and store the time when the program will be finished. More or less 3hours
#we considered to stop the program 4 minutes before the end. Considering the startup time
future_date = datetime.now() + timedelta(minutes=176)

#message  speed
scroll_speed2 = 0.05

#pause between interations. 10 seconds. 
sleep_time = 10 

#initializing SenseHat and rotate the screen
sense = SenseHat()
sense.set_rotation(270)

# show that the program is starting
sense.show_message("Initializing" , scroll_speed = scroll_speed2) 

#define file path and name
base_folder = Path(__file__).parent.resolve()
data_file = base_folder/"data/data.csv"
#create the file
create_csv_file(data_file)

#create a log file
logfile(base_folder/"data/events.log")


#log start program
logger.info(f"program start at {datetime.now()}")

# run the program. It will run until it reaches the time calculated to end
while datetime.now() < future_date: 
    try:
        #show that is reading the sensor data   
        sense.show_message("Reading data" , scroll_speed = scroll_speed2)

        #record the time meausre
        date_iteration = datetime.now()
        #reading humidity, temperature, pressure
        humidity = sense.humidity 
        temperature = sense.temperature  
        pressure = sense.pressure  
        cpu_temp = CPUTemperature()

        #round data
        humidity = round(humidity, 3)
        temperature = round(temperature, 3)
        pressure = round(pressure, 3)

        #show the data using different color depending on temperature
        if temperature > 22 :
            sense.show_message( "H: " + str(humidity) + "  T: " + str(temperature) + "  P: " + str(pressure), scroll_speed = scroll_speed2 , text_colour=[225,0,0])
        else :
            sense.show_message( "H: " + str(humidity) + "  T: " + str(temperature) + "  P: " + str(pressure), scroll_speed = scroll_speed2 , text_colour=[0,128,128])

        # increase the counter
        counter1 = counter1 + 1 

        #retrive IIS position
        location = ISS.coordinates()
        #create data structure
        data = (
                counter1,
                date_iteration,
                location.latitude.degrees,
                location.longitude.degrees,
                temperature,
                humidity,
                pressure, 
                cpu_temp.temperature
                )
        #writing data
        add_csv_data(data_file, data)

        #log iteration
        logger.info(f"recorder iteration {counter1} at {date_iteration}")

        #wait for next iteration
        sleep(sleep_time)
    except Exception as e:
        #log Exception
        logger.error(f'{e.__class__.__name__}: {e}')

#log the end of activities
logger.info(f"program ended at {datetime.now()}")

#show that the program is ended
sense.show_message("Program ends. Thanks!" , scroll_speed = scroll_speed2)